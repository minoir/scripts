#!/usr/bin/env python

import os, sys, subprocess, time, socket, schedule
from subprocess import PIPE, Popen
from threading import Thread
from datetime import datetime
from threading import Timer
from threading import Thread

def memory():
    global tot_m
    global used_m
    global free_m
    global CPU
    CPU=str(round(float(os.popen('''grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage }' ''').readline()),2))
    tot_m, used_m, free_m = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])

def list():
    global port
    while True:
        time.sleep(10800)
        x=1
        while x <= 14:
            a=cmdline("chat -V -s '' AT+CPOL=" + str(x) + " '' > /dev/" + port + " < /dev/" + port)
            x=x+1


def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


def TDK():
    global tdk
    time.sleep(2)
    a=cmdline("openssl x509 -noout -subject -in /boot/client.crt")
    if "/" in a:
        b=a.split("CN")[1]
        c=b.split("/")[0]
        tdk=c.split("=")[1]
        print tdk
    elif "/" not in a:
        b=a.split("CN")[1]
        c=b.split("= ")[1]
        tdk=c.split(",")[0]
        print tdk


def status_connection():
    global tdk
    global LOC
    global LAC
    memory()
    global free_m
    global used_m
    global CPU
    puerto()
    global carrier
    global signal
    global operatorid
    loc()
    global CID
    global LAC
    global mnc
    st= '"mem_used":' + str(used_m) + ',"mem_free":' + str(free_m) + ',"cpu":' + str(CPU) + '","mnc":' + mnc[0:3] + ',"mno":"' + mnc[3:] + '","signal_strength":' + str(signal) + ',"cell_id":' + str(CID) + ',"lac":' + str(LAC)
    a="mosquitto_pub -d -h telematics.nimanic.com -p 8883 -t" + " '" + tdk + "/td_status' -m '" + '{' + '"id":123,' + '"mem_used":' + str(used_m) + ',"mem_free":' + str(free_m) + ',"cpu":' + str(CPU) + ',"operator":' + '"' + str(carrier) + '","mnc":' + mnc[0:3] + ',"mno":"' + mnc[3:] + '","signal_strength":' + str(signal) + ',"cell_id":' + str(CID) + ',"lac":' + str(LAC) + "}'" + " --cafile /boot/ca.crt --cert /boot/client.crt --key /boot/client.key"
    cmdline(a)

def buscar_modem():
    global b
    global c
    if b == "No":
        time.sleep(2)
        modem()
    elif b == "Found":
        a = cmdline("mmcli -L")
        b=a.split()[3]
        c=b.split('/')[5] # Modem
        d="mmcli -m " + c + " --enable"
        cmdline(d)
        puerto()

def modem():
    global b
    a = cmdline("mmcli -L")
    b = a.split()[0]
    buscar_modem()

def loop():
    while True:
        time.sleep(10)
        modem()
        status_connection()
        time.sleep(3590)
        cmdline("systemctl restart cpu")

def is_connected(REMOTE_SERVER):
    try:
        host = socket.gethostbyname(REMOTE_SERVER)
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False

def test_connection():
    print is_connected("www.google.com")
    print is_connected("www.bing.com")
    print is_connected("www.amazon.com")

def puerto():
    time.sleep(2)
    global port
    global carrier
    global operatorid
    global signal
    global c
    global mnc
    port=(((cmdline("mmcli -m " + c)).split('ports:')[1]).split(",")[2]).split()[0]
    carrier=((cmdline("mmcli -m " + c)).split("operator name")[1]).split("'")[1]
    operatorid=((cmdline("mmcli -m " + c)).split("operator id")[1]).split("'")[1]
    mnc= str(operatorid)
    signal=((cmdline("mmcli -m " + c)).split("signal quality")[1]).split("'")[1]

def loc():
    global port
    global LAC
    global CID
    time.sleep(2)
    cmdline("chat -V -s '' AT+CREG=2 '' > /dev/" + port + " < /dev/" + port)
    cmdline("rm /boot/info.txt")
    time.sleep(2)
    cmdline("cat /dev/" + port + " >> /boot/info.txt &")
    time.sleep(2)
    cmdline("chat -V -s '' AT+CREG? '' > /dev/" + port + " < /dev/" + port)
    cmdline("killall cat")
    time.sleep(2)
    a=cmdline("more /boot/info.txt")
    LAC=0
    CID=0
    print LAC
    print CID
    print a
    LAC=int(((a.split(',')[2]).split()[0]),16) #DEC
    CID=int(((a.split(',')[3]).split()[0]),16) #DEC
    print LAC
    print CID


def job():
    global tdk
    cron="mosquitto_pub -d -h telematics.nimanic.com -p 8883 -t" + " '" + tdk + "/status' -m '" + '{' + '"online":0' + ',"reason":"schedule_restart"' + "}'" + " --cafile /boot/ca.crt --cert /boot/client.crt --key /boot/client.key"
    cmdline(cron)
    return

def cron_():
    time.sleep(2)
    schedule.every().day.at("04:19").do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)

def stat():
    TDK()
    modem()
    loop()

tdk=""
Thread(target = stat).start()
Thread(target = cron_).start()
#YaNoSoyUnaNota