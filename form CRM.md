
```
sudo nano /home/ubuntu/start.sh
```
Con el contenido

```
#! /bin/bash
sudo python3 /home/ubuntu/recruitmentproject/RECRUITMENT_PROJECT/main_program.py
```
darle permisos con
```
sudo chmod 755 /home/ubuntu/recruitmentproject/RECRUITMENT_PROJECT/main_program.py
sudo chmod 755 /home/ubuntu/start.sh
```

en el directorio 


```
/etc/systemd/system
```
crear un archivo que se llame
```
form.service
```

con el siguiente contenido

```
[Unit]
Description=Form
After=network.target
[Service]
ExecStart=/home/ubuntu/start.sh
KillMode=process
Restart=always
[Install]
WantedBy=multi-user.target

```

Ctrl + x para guardar
```
systemctl daemon-reload
systemctl enable form.service
systemctl start form.service
```


```
/usr/bin/python3 /home/ubuntu/recruitmentproject/RECRUITMENT_PROJECT/main_program.py
```